[![Netlify Status](https://api.netlify.com/api/v1/badges/89eac6b7-d405-4d1d-ad80-4c0c9120a6f8/deploy-status)](https://app.netlify.com/sites/jovial-wiles-f1737f/deploys)

Jekyll FullIt Theme 
======================

Publish your static website/blog on GitHub Pages using [Jekyll](https://jekyllrb.com/), [Bootstrap 4](https://github.com/twbs/bootstrap/tree/v4-dev) and [fullpage.js](https://github.com/alvarotrigo/fullPage.js/).

## Usage

### 1. Create a Repo
- Go to <https://github.com> and create a new repository named *USERNAME.github.io*  

### 2. Install Jekyll-Bootstrap-3  
<pre>
  <code>
    $ git clone https://github.com/fullit/fullit.github.io USERNAME.github.io
    $ cd USERNAME.github.io
    $ git remote set-url origin git@github.com:USERNAME/USERNAME.github.io.git
    $ git push origin master  
  </code>
</pre> 

### 3. Edit your website configuration

Edit `_includes/themes/bootstrap/` to change information about your new website and also to remove the GitHub red ribbon ([default.html#26](https://fulliit.github.io)).

### 4. Enjoy !
- After giving 10 mins to GitHub of course.  

## Demo

Visit [Jekyll-Fullit](https://fullit.github.io) on GitHub Pages


## License

[MIT](http://opensource.org/licenses/MIT)
